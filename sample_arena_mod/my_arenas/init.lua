
local modpath = minetest.get_modpath("my_arenas")
local schemdir = modpath.."/schems/"




bb_schems.register_arena({
    
    name = "My Special Arena", --the human readable name of the arenas
    author = "YourName", --the author of the arenas
    schem = schemdir.."myarena.mts", --the replace with the name of the schematic
    min = 2, --minimum players
    max = 4, --maximum players
    spawns = {
        vector.new(x,y,z), -- the spawn locations, needs as many as max players
        
    }, --a table of spawn locations
    spectator_pos = vector.new(x,y,z), -- spectator position, should be above and in the center of the arena.
})
