local storagepath = minetest.get_worldpath().."/blockbombermods"
local s = minetest.get_mod_storage()
editor = {}

minetest.register_node("editor:wall",{
    description = "wall",
    paramtype = "light",
    tiles = {{name = "editor_grid.png", align_style = "world", scale = 4}},
    groups = {not_in_creative_inventory = 1},
})

minetest.register_node("editor:red",{
    description = "wall",
    paramtype = "light",
    tiles = {"red.png"},
    groups = {not_in_creative_inventory = 1},
})



minetest.register_on_generated(function(minp, maxp)

    local vm, emin, emax = minetest.get_mapgen_object("voxelmanip")
    local data = vm:get_data()
    local area = VoxelArea:new({MinEdge=emin, MaxEdge=emax})
    local c_red = minetest.get_content_id("editor:red")
    local c_block = minetest.get_content_id("editor:wall")

    for x = minp.x, maxp.x do
        for y = minp.y, maxp.y do
            for z = minp.z, maxp.z do
                local p_pos = area:index(x, y, z)

                local pos = vector.new(x,y,z)
                if pos == vector.new(-1,0,0) or pos == vector.new(0,-1,0) or pos == vector.new(0,0,-1) then
                    data[p_pos] = c_red
                elseif pos.x < 0 or pos.y < 0 or pos.z < 0 then
                    data[p_pos] = c_block
                end

            end
        end
    end

    vm:set_data(data)
    vm:calc_lighting()
    vm:update_liquids()
    vm:write_to_map()

end)

minetest.register_on_joinplayer(function(player, last_login)

    player:set_pos(vector.new(5,5,5))
    local inv = player:get_inventory()
    if not inv:contains_item("main", "bb_nodes:breaker") then
        inv:add_item("main", "bb_nodes:breaker")
    end
    player:hud_set_hotbar_itemcount(10)

end)

minetest.register_on_mods_loaded(function()
    -- for name,def in pairs(minetest.registered_nodes) do
    --     if minetest.get_item_group(name, "floor") > 0 then
    --         minetest.override_item(name, {
                
    --             on_construct = function(pos)
    --                 pos = vector.new(pos.x,pos.y,pos.z)
    --                 if pos.y ~= 0 then
    --                     minetest.set_node(pos,{name = "air"})
    --                 end
    --             end,
    --         })
    --     else
    --         minetest.override_item(name, {
                
    --             on_construct = function(pos)
    --                 pos = vector.new(pos.x,pos.y,pos.z)
    --                 if pos and pos.y and pos.y ~= 1 then
    --                     minetest.set_node(pos,{name = "air"})
    --                 end 
    --             end,

    --         })
    --     end

    -- end

    for name,def in pairs(minetest.registered_items) do
        
        minetest.override_item(name, {
            range = 30.0,
        })
    

    end

end)


-- minetest.register_on_placenode(function(pos, newnode, placer, oldnode, itemstack, pointed_thing)

--     local name = newnode.name
--     local floor = minetest.get_item_group(name, "floor") > 0
--     if floor then 
--         if pos.y ~= 0 then
--             minetest.set_node(pos,{name = "air"})
--         end
--     else
--         if pos.y ~= 1 then
--             minetest.set_node(pos,{name = "air"})
--         end
--     end


-- end)

